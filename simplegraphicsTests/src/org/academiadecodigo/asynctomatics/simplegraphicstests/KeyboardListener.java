package org.academiadecodigo.asynctomatics.simplegraphicstests;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyboardListener implements KeyboardHandler {

    public void setup() {

        Keyboard myKeyboard = new Keyboard(this);
        KeyboardEvent myEvent = new KeyboardEvent();

    }

    @Override
    public void keyPressed(KeyboardEvent var1) {

    }

    @Override
    public void keyReleased(KeyboardEvent var1) {

    }
}
