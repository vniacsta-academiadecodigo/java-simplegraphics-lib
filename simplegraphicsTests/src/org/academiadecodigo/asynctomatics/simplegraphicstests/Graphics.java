package org.academiadecodigo.asynctomatics.simplegraphicstests;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Graphics {

    public static void main(String[] args) {

        // define the size of my terminal
        Rectangle canvas = new Rectangle(10, 10, 600, 400);
        canvas.draw();
        canvas.fill();
        canvas.setColor(new Color(117, 90, 102));

        Rectangle computerKeyboard = new Rectangle(150, 180, 300, 180);
        computerKeyboard.draw();
        computerKeyboard.fill();
        computerKeyboard.setColor(new Color(173, 173, 173));

        Rectangle computerScreen = new Rectangle(150, 140, 300, 50);
        computerScreen.draw();
        computerScreen.fill();
        computerScreen.setColor(Color.WHITE);

        int numberKeys = 10;
        int x = 160;
        Rectangle[] keys = new Rectangle[numberKeys];
        Text[] keyLetter = new Text[10];

        for (int i = 0; i < numberKeys; i++) {
            keys[i] = new Rectangle(x, 220, 20, 20);
            keys[i].draw();
            keyLetter[i] = new Text(x + 5, 220, "P");
            keyLetter[i].draw();

            x += 8;

            for (int i1 = 0; i1 < numberKeys; i1++) {
                keys[i1] = new Rectangle(x, 245, 20, 20);
                keys[i1].draw();
                keyLetter[i1] = new Text(x + 5, 245, "A");
                keyLetter[i1].draw();

                x += 8;

                for (int i2 = 0; i1 < numberKeys; i1++) {
                    keys[i2] = new Rectangle(x, 270, 20, 20);
                    keys[i2].draw();
                    keyLetter[i2] = new Text(x + 5, 270, "U");
                    keyLetter[i2].draw();

                    x += 5;

                    for (int i3 = 0; i1 < numberKeys; i1++) {
                        keys[i3] = new Rectangle(x, 295, 20, 20);
                        keys[i3].draw();
                        keyLetter[i3] = new Text(x + 5, 295, "L");
                        keyLetter[i3].draw();

                        x += 5;

                        for (int i4 = 0; i1 < numberKeys; i1++) {
                            keys[i4] = new Rectangle(x, 320, 20, 20);
                            keys[i4].draw();
                            keyLetter[i4] = new Text(x + 5, 320, "O");
                            keyLetter[i4].draw();
                        }
                    }
                }
            }
        }

        Text title = new Text(230, 100, "Who's Apple Fanboy? xD");
        title.draw();
        title.grow(0, 10);

        Picture appleLogo = new Picture(275, 142, "resources/apple-logo.png");
        appleLogo.draw();

        Picture stallman = new Picture(470, 30, "resources/stallman1.png");
        stallman.draw();

        Text stallmanText = new Text(465, 165, "Stallman disapproves!");
        stallmanText.draw();

        Text thanks = new Text(215, 380, "We still appreciate you! :wink:");
        thanks.draw();

        new KeyboardListener().setup();

    }
}
